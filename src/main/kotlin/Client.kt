import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.mainBody
import kotlin.system.exitProcess

lateinit var clientSecret: String
lateinit var baseUrl: String

fun main(args: Array<String>) = mainBody {
    val idxAt = args.indexOf("--add-task")
    val idxA = args.indexOf("-a")
    if (idxAt != -1 || idxA != -1) {
        val trueIdxA = idxAt + idxA + 1
        val timeSlice = args.sliceArray(IntRange(trueIdxA + 1, args.lastIndex))
        if (timeSlice.size != args.size) {
            userTasks(args.sliceArray(IntRange(0, trueIdxA - 1)), help = false)
        }
        timeTasks(timeSlice)
    } else {
        userTasks(args)
    }
}

fun userTasks(args: Array<String>, help: Boolean = true) {
    if (args.isEmpty() && help) {
        ArgParser(arrayOf("--help")).parseInto(::Parser)
        exitProcess(0)
    }

    val parsedArgs = ArgParser(args).parseInto(::Parser)

    if (parsedArgs.register.isNotEmpty()) {
        val loginToo = if (parsedArgs.login.isNotEmpty()) true else false
        println("Registering ${if (loginToo) "" else "and logging in"} user ...")
        initUserTempStore(parsedArgs.register)
        registerUser(loginToo)
        exitProcess(0)
    }

    if (parsedArgs.login.isNotEmpty()) {
        println("Logging in user...")
        initUserTempStore(parsedArgs.login)
        logUserIn()
        exitProcess(0)
    }

    if (parsedArgs.status) {
        val commits = readCommits()
        if (commits.isEmpty()) {
            println("Commits up to date, nothing to commit!\n")
        } else {
            println("Commits staged to be pushed:\n")
            commits.forEach {
                println("\t${it.category} : ${it.task}")
            }
        }
    }

    if (parsedArgs.push) {
        if (readCommits().isNotEmpty()) {
            println("Pushing commits...")
            pushCommits(retrieveSecrets())
            println("Secrets pushed successfully!")
        } else {
            println("Nothing to push!")
        }
    }
}

fun timeTasks(args: Array<String>) {
    var err: Boolean = false
    val taskArgs = ArgParser(args)
        .parseInto(::TaskParser)

    val task = taskArgs.task
    val category = taskArgs.category

    if (task.isEmpty()) {
        err = true
        printErr("Task must not be empty!")
    }
    if (category.isEmpty()) {
        err = true
        printErr("Category must not be empty!")
    }
    if (err) exitProcess(1)

    addAndPushCommits(retrieveSecrets(), taskArgs)
}

// Requirement for application plugin
class Client
