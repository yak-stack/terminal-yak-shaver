import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import java.io.File
import java.io.FileReader
import java.io.IOException
import java.nio.file.Files
import java.util.Properties
import kotlin.system.exitProcess
import yakstack.yaklib.Obj.CommitData
import yakstack.yaklib.Obj.TimeCommit
import yakstack.yaklib.Obj.Token
import yakstack.yaklib.YakLib

fun retrieveSecrets(): Secrets {
    if (!File(SECRET_STORE).exists()) {
        printErr("User has not logged in. Please log in with the --login flag!")
        exitProcess(5)
    }
    val secretsProp = FileReader(SECRET_STORE)
    val secrets = Properties()
    secrets.load(secretsProp)

    lateinit var loadedSecrets: Secrets

    try {
        loadedSecrets = Secrets(
            secrets[EMAIL] as String,
            secrets[TOKEN] as String,
            secrets[ENDPOINT] as String
        )
    } catch (e: ClassCastException) {
        printErr("Malformed secrets. " +
                "Please log in to retrieve new secrets: \n" +
                "--login <USER_INFO_FILE>")
        exitProcess(2)
    }
    return loadedSecrets
}

fun addAndPushCommits(secrets: Secrets, args: TaskParser) {
    val newCommit = TimeCommit(
            args.category,
            args.task,
            args.startTime.toDate().localeString(),
            args.endTime.toDate().localeString(),
            args.props.toPropertyMap())

    val commits = readCommits()
    commits.add(newCommit)

    send(secrets, commits)
}

fun pushCommits(secrets: Secrets) {
    send(secrets, readCommits())
}

fun send(secrets: Secrets, commits: MutableList<TimeCommit>) {
    if (commits.isEmpty()) {
        println("Nothing to commit!")
        exitProcess(0)
    }

    writeCommits(commits)
    val commitData = CommitData(secrets.email, Token(secrets.token), commits)

    try {
        YakLib(secrets.endpoint).pushCommits(commitData)

        // Clean up after ourselves
        if (File(COMMIT_STORE).exists()) {
            Files.delete(File(COMMIT_STORE).toPath())
        }
    } catch (e: IOException) {
        printErr("Error sending commits to client: \n$e Please try again later.")
        exitProcess(3)
    }
}

fun writeCommits(commits: MutableList<TimeCommit>) {
    val writeableCommits = ListCommits(commits)
    val data = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
            .adapter(ListCommits::class.java)
            .toJson(writeableCommits)

    File(COMMIT_STORE).writeText(data)
}

/**
 * Read uncommitted CommitData
 *
 * @return User's unpushed CommitData
 */
fun readCommits(): MutableList<TimeCommit> {
    // Don't want to err out on an empty file.
    // This just means the user hasn't done anything yet,
    // or they have a good internet connection.
    if (!File(COMMIT_STORE).exists()) {
        return mutableListOf<TimeCommit>()
    }

    val commits = File(COMMIT_STORE).readText()

    val data = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
            .adapter(ListCommits::class.java)
            .fromJson(commits)
    return if (data == null) mutableListOf<TimeCommit>() else data?.commits
}

// Ugh moshi requires a class for deserializing JSON so this stupid thing
// has to exist.
// Please pretend it doesn't exist and never use it if you have the option.
private data class ListCommits(val commits: MutableList<TimeCommit>)
