data class Secrets(val email: String, val token: String, val endpoint: String)

data class LoginSecrets(val email: String, val password: String)
