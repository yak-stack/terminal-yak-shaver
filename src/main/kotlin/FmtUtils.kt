import java.text.SimpleDateFormat
import java.util.Date
import yakstack.yaklib.Obj.DATE_FORMAT

fun printErr(errorMsg: String) {
    System.err.println(errorMsg)
}

fun Date.localeString(): String {
    val format = SimpleDateFormat(DATE_FORMAT)
    return format.format(this)
}
