import kotlin.system.exitProcess
import java.io.FileOutputStream
import java.io.FileReader
import java.io.IOException
import java.io.File
import java.net.InetAddress
import java.nio.file.Files
import java.nio.file.Paths
import java.util.Properties
import yakstack.yaklib.Obj.ReturnCode.INVALID_PASSWORD
import yakstack.yaklib.Obj.ReturnCode.SUCCESS
import yakstack.yaklib.Obj.ReturnCode.USER_DOES_NOT_EXIST
import yakstack.yaklib.Obj.UserLoginInfo
import yakstack.yaklib.Obj.UserGivenInfo
import yakstack.yaklib.YakLib

fun initUserTempStore(userFile: String) {
    if (!File(TIME_CLIENT_STORE).isDirectory()) {
        println("Setting up time client directories...")
        File(TIME_CLIENT_STORE).mkdirs()
    }
    if (!File(userFile).exists()) {
        printErr("Must supply user configuration file!")
        printErr("Given path: $userFile")
        exitProcess(1)
    }

    if (File(USER_TMP_STORE).exists()) {
        Files.delete(Paths.get(USER_TMP_STORE))
    }
    File(userFile).copyTo(File(USER_TMP_STORE))
}

fun registerUser(login: Boolean = false) {
    val uinfo = retrieveFromUserTempStore(
        UserConfigType.REGISTER,
        deleteInfo = false
    )
    try {
        val returnCode = YakLib(uinfo.endpoint).registerUser(
            uinfo.uinfo.email,
            uinfo.uinfo.password,
            uinfo.uinfo.first_name as String,
            uinfo.uinfo.last_name as String
        )
        print("${returnCode.msg}")
    } catch (e: IOException) {
        printErr("Error registering: $e\nPlease try again later.")
        exitProcess(3)
    }
    if (login) {
        logUserIn()
    }
}

fun logUserIn() {
    try {
        val uinfo = retrieveFromUserTempStore(UserConfigType.LOGIN)
        val loginResponse = YakLib(uinfo.endpoint)
                .loginUser(uinfo.uinfo.email, uinfo.uinfo.password)

        when (loginResponse.returnCode) {
            SUCCESS -> {
                println("Log in success! Storing to secrets file...")
                insertSecrets(loginResponse.userLoginInfo, uinfo.endpoint)
            }
            USER_DOES_NOT_EXIST -> {
                println("User does not exist. Please register as a user.")
                exitProcess(4)
            }
            INVALID_PASSWORD -> {
                println("Invalid username or password.")
                exitProcess(4)
            }
            else -> {
                println("An unkown error occurred. Please contact your local YakStack maintainers.")
                exitProcess(254)
            }
        }
    } catch (e: IOException) {
        printErr("Error logging in: $e\nPlease try again later.")
        exitProcess(3)
    }
}

private fun insertSecrets(userInfo: UserLoginInfo, endpoint: String) {
    val userProperties = Properties()
    userProperties.put(TOKEN, userInfo.token.token)
    userProperties.put(EMAIL, userInfo.email)
    userProperties.put(FNAME, userInfo.first_name)
    userProperties.put(LNAME, userInfo.last_name)
    userProperties.put(ENDPOINT, endpoint)

    val pOut = FileOutputStream(SECRET_STORE)
    userProperties.store(pOut, "User secrets")
    println("Secrets saved successfully! Time client is ready for use! ✓")
}

/**
 *  Get user given info from file and delete file afterwards.
 */
private fun retrieveFromUserTempStore(
    configType: UserConfigType,
    deleteInfo: Boolean = true
): ExtendedUserGivenInfo {

    val uinfoProp = FileReader(USER_TMP_STORE)
    val uinfos = Properties()
    uinfos.load(uinfoProp)
    if (deleteInfo) Files.delete(Paths.get(USER_TMP_STORE))

    try {
        return when (configType) {
            UserConfigType.REGISTER -> {
                ExtendedUserGivenInfo(
                    uinfos[ENDPOINT] as String,
                    UserGivenInfo(
                        uinfos[EMAIL] as String,
                        uinfos[PASSWORD] as String,
                        uinfos[FNAME] as String,
                        uinfos[LNAME] as String,
                        null
                    )
                )
            }
            UserConfigType.LOGIN -> {
                val devName = uinfos[DEV_NAME] ?: InetAddress.getLocalHost().getHostName() + "'s terminal client"
                ExtendedUserGivenInfo(
                    uinfos[ENDPOINT] as String,
                    UserGivenInfo(
                        uinfos[EMAIL] as String,
                        uinfos[PASSWORD] as String,
                        null,
                        null,
                        devName as String
                    )
                )
            }
        }
    } catch (e: ClassCastException) {
        e.printStackTrace()
        printErr("Malformed user config file. Please check syntax.")
        exitProcess(2)
    }
}
