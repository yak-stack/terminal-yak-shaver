import yakstack.yaklib.Obj.UserGivenInfo

data class ExtendedUserGivenInfo(
    val endpoint: String,
    val uinfo: UserGivenInfo
)
